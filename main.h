#include <16F690.h>
#device *=16
#device ADC=10

#FUSES WDT                    //No Watch Dog Timer
#FUSES PUT                      //Power Up Timer
#FUSES BROWNOUT               //No brownout reset

#use delay(crystal=4000000)
#define analogIN   PIN_A2
#define IN4   PIN_B4
#define IN5   PIN_B6
#define IN2   PIN_C3
#define IN1   PIN_C4
#define pwmOUT   PIN_C5
#define IN3   PIN_C6

#use rs232(baud=57600,parity=N,xmit=PIN_B7,rcv=PIN_B5,bits=8,stream=PLstream,ERRORS)

