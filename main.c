#include <main.h>
#include "PL.c"

const int16 waitTime = 40;

// header function
void sendSettings();
void save_setting();
void load_setting();

//global
int1 mode_input1 = 0;
int1 mode_input2 = 0;
int1 mode_input3 = 0;
int1 mode_input4 = 0;
int1 mode_input5 = 0;
int1 mode_V_enable = 1;
int16 mode_V_level_hi = 676;
int16 mode_V_level_lo = 650;
int16 V_volume = 0;
int16 PWM_level = 300;
int16 threshold_level = 200;

int1 mode_PWM_test = 0;
int1 mode_voltage_send = 0;
int1 enable_check_states = 0;
int16 count_timer1 = 0;
int8 V_State = 1;
int16 PWM_count = 0;
int16 PWM_target = 0;
int16 threshold_count = 0;
int16 timer_count = 0;

void OnReadData(byte channel) {
    mode_voltage_send = 1;
    switch (channel) {
        case 0:
            sendSettings();
            break;
        case 1:
            mode_input1 = PLGetInt8();
            break;
        case 2:
            mode_input2 = PLGetInt8();
            break;
        case 3:
            mode_input3 = PLGetInt8();
            break;
        case 4:
            mode_input4 = PLGetInt8();
            break;
        case 5:
            mode_input5 = PLGetInt8();
            break;
        case 6:
            mode_V_enable = PLGetInt8();
            break;
        case 7:
            mode_V_level_hi = PLGetInt16();
            break;
        case 8:
            mode_V_level_lo = PLGetInt16();
            break;
        case 9:
            PWM_level = PLGetInt16();
            break;
        case 10:
            threshold_level = PLGetInt16();
            break;
        case 11:
            save_setting();
        case 15:
            mode_voltage_send = 0;
            break;
    }

    if (channel == 9) mode_PWM_test = 1;
    else mode_PWM_test = 0;
}

void sendSettings() {
    PLSendInt8(1, mode_input1);
    PLSendInt8(2, mode_input2);
    PLSendInt8(3, mode_input3);
    PLSendInt8(4, mode_input4);
    PLSendInt8(5, mode_input5);
    PLSendInt8(6, mode_V_enable);
    PLSendInt16(7, mode_V_level_hi);
    PLSendInt16(8, mode_V_level_lo);
    PLSendInt16(9, PWM_level);
    PLSendInt16(10, threshold_level);
    PLSendInt16(12, V_volume);
    PLSendInt8(0, 1);
}

void sendInputs()
{
    int8 inp = 0;
    if (input_state(IN1)) bit_set(inp, 0);
    if (input_state(IN2)) bit_set(inp, 1);
    if (!input_state(IN3)) bit_set(inp, 2);
    if (input_state(IN4)) bit_set(inp, 3);
    if (!input_state(IN5)) bit_set(inp, 4);
    PLSendInt8(14, inp);
}

void Threshold() {
    threshold_count++;
    if (threshold_count > threshold_level) {
        if (PWM_count < PWM_target && timer_count >= waitTime) {
            PWM_count++;
            set_pwm1_duty(PWM_count);
        }
        if (PWM_count > PWM_target) {
            PWM_count--;
            set_pwm1_duty(PWM_count);
        }
        threshold_count = 0;
    }
}

void checkState() {
    V_volume = read_adc();

    if (mode_V_enable) {
        if (V_volume > mode_V_level_hi) V_State = 0;
        if (V_volume < mode_V_level_lo) V_State = 1;
    } else V_State = 0;

    int1 outState = 0;
    outState = outState || (mode_input1 && input_state(IN1)) ||
            (mode_input2 && input_state(IN2)) ||
            (mode_input3 && !input_state(IN3)) ||
            (mode_input4 && input_state(IN4)) ||
            (mode_input5 && !input_state(IN5)) || V_State;
            
    if (!outState && timer_count < waitTime) timer_count ++;
    if (outState && timer_count > 0) timer_count = 0;       

    if (!outState || mode_PWM_test) PWM_target = PWM_level;
    else PWM_target = 0; 

    if (mode_voltage_send == 1) {
        count_timer1++;
        if (count_timer1 == 10) {
            PLSendInt16(12, V_volume);
            PLSendInt16(13, PWM_count);
            sendInputs();
            count_timer1 = 0;
        }
    }
    enable_check_states = 0;
}

void load_setting() {
    if (read_eeprom(1) == 0xFF) return;
    mode_input1 = read_eeprom(1);
    mode_input2 = read_eeprom(2);
    mode_input3 = read_eeprom(3);
    mode_input4 = read_eeprom(4);
    mode_input5 = read_eeprom(5);
    mode_V_enable = read_eeprom(6);
    mode_V_level_hi = make16(read_eeprom(7), read_eeprom(8));
    mode_V_level_lo = make16(read_eeprom(9), read_eeprom(10));
    PWM_level = make16(read_eeprom(11), read_eeprom(12));
    threshold_level = make16(read_eeprom(13), read_eeprom(14));
}

void save_setting() {
    write_eeprom(1, mode_input1);
    write_eeprom(2, mode_input2);
    write_eeprom(3, mode_input3);
    write_eeprom(4, mode_input4);
    write_eeprom(5, mode_input5);
    write_eeprom(6, mode_V_enable);
    write_eeprom(7, (mode_V_level_hi >> 8));
    write_eeprom(8, (mode_V_level_hi & 0xFF));
    write_eeprom(9, (mode_V_level_lo >> 8));
    write_eeprom(10, (mode_V_level_lo & 0xFF));
    write_eeprom(11, (PWM_level >> 8));
    write_eeprom(12, (PWM_level & 0xFF));
    write_eeprom(13, (threshold_level >> 8));
    write_eeprom(14, (threshold_level & 0xFF));
    PLSendInt8(11, 1);
}

#INT_TIMER1

void TIMER1_isr(void) {
    enable_check_states = 1;
}

void main() {

    setup_wdt(WDT_ON);
    setup_wdt(WDT_2304MS | WDT_DIV_2); //~1,1 s reset

    port_B_pullups(0x50);
    setup_adc_ports(sAN2);
    setup_adc(ADC_CLOCK_DIV_2);

    setup_timer_1(T1_INTERNAL | T1_DIV_BY_2); //104 ms overflow
    setup_timer_2(T2_DIV_BY_16, 255, 1); //250hz 45 us overflow
  //  setup_timer_2(T2_DIV_BY_4,124,15);      //100 us overflow, 1,5 ms interrupt
  //  setup_timer_2(T2_DIV_BY_1,199,1);      //40,0 us overflow, 40,0 us interrupt
    setup_ccp1(CCP_PWM | CCP_SHUTDOWN_AC_L | CCP_SHUTDOWN_BD_L);

    PLOnReadData = OnReadData;
    PLInit();

    set_adc_channel(2);
    read_adc(ADC_START_ONLY);
    set_pwm1_duty((int16) 0);

    load_setting();
    
    enable_interrupts(INT_TIMER1);
    enable_interrupts(GLOBAL);    

    while (TRUE) {
        PLCheckMess();
        rs232_errors = 0;
        if (enable_check_states) checkState();
        Threshold();
        restart_wdt();
    }

}
